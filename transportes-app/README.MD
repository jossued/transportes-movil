# Credenciales

Credenciales Google

```
Username: tbm2080@gmail.com
Password: TBM2100$$
```

Credenciales de la firma Android Google play store
```
Key Store Password: Joaquin22
Key alias: alias_name
Key Password: Joaquin22
```

Credenciales de la cuenta de apple developers
```
correo: info@tbm.com.ec
password: TBMpiso82018
```

## Desplegar ANDROID

### Crear apk

1. Desplegar APK 

```
$ ionic build --prod
$ npx cap copy
$ npx cap sync
$ npx cap open android
```
2. CONSTRUIR APK CON FIRMA

Copiar
```
/llaves
    /android
        -> my-release-key.keystore
```
En
```
/android
    /app
        /build
            /outputs
                /apk
                    /debug
                        -> my-release-key.keystore
```

2.Cambiar id de la aplicacion

Dirigirse al menu:
 
```
/File
    /Project Structure
```
Luego a:

```
/Modules
    /app
        /Default Config
```
y Cambiar los valores: 

```
Application Id: com.victoriacreativelabs.alitasdelcadilac
Version Code: X
Version Name: X.X
```


3. Generar Bundle / Apk firmada

Ir al menu de android:
```
/build
    /Generate Signed Bundle / APK...
```

Luego seleccionar

```
- Android App Bundle
```

-   Seleccionar el archivo keystore
-   Llenar las llaves con las credenciales de Google Play Store

Finalmente seleccionar `Release`

### Subir a la tienda

Dirigerse a `Google Play Console`

1. Release Management / App releases / Open Track -> Beta Management

2. Create Release

3. Browse Files

4. Seleccionar el archivo

```
/android
    /app
        /release
            -> app.aab
```

5. Poner descripcion

6. Guardar

## Desplegar IOS

### Crear ipa

1. Construir
   
```
$ ionic build --prod
$ npx cap copy
$ npx cap sync
$ npx cap open ios
   ```


2. Abrir el Xcode y verificar que este habilitado en los `Background Modes` la opcion:

```
- Remote notifications 
```

Y las `Push Notifications`

3.Revisar el identificador de la aplicacion

En la pantalla principal `App` en `General` revisar estos valores:

```
Bundle Identifier: com.tbm.alitasdelcadilac
```

Sean iguales que en el archivo `capacitor.config.json`:

```
appId: com.tbm.alitasdelcadilac
```

y que la version concuerde:
```
version: X.X
build: X
```


4. Generar el ipa desde un dispositivo real 

Ir a la opcion 

```
/Product
    /Archive
```

Se abre una ventana y seleccionar la aplicacion

Escoger la opcion `Verify App` y dar siguiente siguiente.

`OJO` Si da errores en las distributions solo crear una nueva.

Luego dar en `Distribute App` y siguiente siguiente.

5. Ingresar a Apple Developers con las credenciales de Apple Developers.

Ir a 

```
/App Store Connect
    /Ir a app store connect
```

Luego a 

```
/My apps
    /Alista del cadilac
```

