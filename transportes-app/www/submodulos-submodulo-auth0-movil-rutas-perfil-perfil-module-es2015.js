(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["submodulos-submodulo-auth0-movil-rutas-perfil-perfil-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.html":
/*!***********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.html ***!
  \***********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"warning\">\n        <ion-title class=\"ion-text-center\">\n            <img class=\"icono-arriba icono-arriba-espacio-modal\" src=\"assets/imagenes/logo-alitas-cabecera.svg\" alt=\"logo\">\n        </ion-title>\n        <ion-buttons slot=\"primary\">\n            <ion-button (click)=\"dismiss()\">\n                <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n    <div *ngIf=\"!aceptoUbicacion\" class=\"animated slideInUp\">\n        <h3>Paso 1 - Selecciona tu ubicación</h3>\n        <p>Ayúdanos con los datos de tu ubicación para que puedas realizar pedidos, <span class=\"bold\">se lo más exacto posible</span>.\n            Arrastra el\n            <img class=\"marcador\" src=\"assets/imagenes/marcador-direccion.svg\" alt=\"\">\n            hasta tu dirección exacta.\n        </p>\n    </div>\n<!--    <app-seleccionar-posicion *ngIf=\"!aceptoUbicacion\"-->\n<!--                              class=\"animated slideInUp\"-->\n<!--                              [seleccionarDentroDePoligono]=\"true\"-->\n<!--                              [mostrarPoligono]=\"false\"-->\n<!--                              [zoom]=\"19\"-->\n<!--                              (estaDentroDeAlgunPoligono)=\"seleccionoPunto($event)\"-->\n<!--    ></app-seleccionar-posicion>-->\n    <div *ngIf=\"!aceptoUbicacion\" class=\"animated slideInUp\">\n        <p><span class=\"bold\">Importante:</span> Recuerda que no trabajamos en todos los sectores del Ecuador.</p>\n        <p>Tú eres el punto de color <span class=\"rosado bold\">rosado</span>.</p>\n        <p *ngIf=\"ubicacionSeleccionada\">Ubicación: <span\n                class=\"bold\">{{ubicacionSeleccionada[0].toString().slice(0, 10)}} {{ubicacionSeleccionada[1].toString().slice(0, 10)}}</span>\n        </p>\n        <ion-button\n                class=\"animated slideInUp\"\n                expand=\"block\"\n                color=\"primary\"\n                [disabled]=\"!ubicacionSeleccionada\"\n                (click)=\"aceptarUbicacion()\">\n            Aceptar ubicación\n        </ion-button>\n    </div>\n    <ion-grid class=\"animated fadeInLeft\" *ngIf=\"aceptoUbicacion\">\n        <ion-row>\n            <ion-col size=\"4\">\n                <img src=\"assets/imagenes/completa-direccion.svg\" style=\"max-height: 92px;\">\n            </ion-col>\n            <ion-col>\n                <h3 class=\"titulo animated fadeInUp\">\n                    Paso 2 - Completa tu dirección\n                </h3>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <p class=\"ion-text-justify\">\n                    Ayúdanos con los datos de tu dirección\n                    <span class=\"bold\">\n                        lo más completo posible</span>, así podremos llegar rápidamente con tu pedido.\n                </p>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-grid>\n                    <!--                calle principal-->\n                    <ion-row>\n                        <ion-col>\n                            <ion-label class=\"form-input\">\n                                *Calle principal :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"callePrincipal.errors && callePrincipal.dirty && callePrincipal.touched\"\n                                          (click)=\"mostrarToast('Ingresa calle principal entre 4 y 100 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"Av 10 de Agosto\"\n                                    required\n                                    inputmode=\"text\"\n                                    minlength=\"4\"\n                                    maxlength=\"100\"\n                                    name=\"callePrincipal\"\n                                    #callePrincipal=\"ngModel\"\n                                    [(ngModel)]=\"direccionACrear.callePrincipal\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--                calle secundaria-->\n                    <ion-row>\n                        <ion-col>\n                            <ion-label class=\"form-input\">\n                                *Calle secundaria :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"calleSecundaria.errors && calleSecundaria.dirty && calleSecundaria.touched\"\n                                          (click)=\"mostrarToast('Ingresa calle principal entre 4 y 100 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"Naciones unidas\"\n                                    required\n                                    inputmode=\"text\"\n                                    minlength=\"4\"\n                                    maxlength=\"100\"\n                                    name=\"calleSecundaria\"\n                                    #calleSecundaria=\"ngModel\"\n                                    [(ngModel)]=\"direccionACrear.calleSecundaria\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!-- numero-calle-->\n                    <ion-row>\n                        <ion-col>\n                            <ion-label class=\"form-input\">\n                               * Número de domicilio :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"numeroCalle.errors && numeroCalle.dirty && numeroCalle.touched\"\n                                          (click)=\"mostrarToast('Ingresa calle principal entre 4 y 100 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"OE4-23\"\n                                    inputmode=\"text\"\n                                    minlength=\"3\"\n                                    required\n                                    maxlength=\"50\"\n                                    name=\"numeroCalle\"\n                                    #numeroCalle=\"ngModel\"\n                                    [(ngModel)]=\"direccionACrear.numeroCalle\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--                    codigo postal-->\n                    <ion-row>\n                        <ion-col>\n                            <ion-label class=\"form-input\">\n                                Código postal :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"codigoPostal.errors && codigoPostal.dirty && codigoPostal.touched\"\n                                          (click)=\"mostrarToast('Ingresa código postal.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"170176\"\n                                    inputmode=\"numeric\"\n                                    name=\"codigoPostal\"\n                                    #codigoPostal=\"ngModel\"\n                                    [(ngModel)]=\"direccionACrear.codigoPostal\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--                nombreEdificio-->\n                    <ion-row>\n                        <ion-col>\n                            <ion-label class=\"form-input\">\n                                Nombre edificio :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"nombreEdificio.errors && nombreEdificio.dirty && nombreEdificio.touched\"\n                                          (click)=\"mostrarToast('Ingresa nombre del edificio.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"Platinium III\"\n                                    inputmode=\"text\"\n                                    name=\"nombreEdificio\"\n                                    #nombreEdificio=\"ngModel\"\n                                    [(ngModel)]=\"direccionACrear.nombreEdificio\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--                piso-->\n                    <ion-row>\n                        <ion-col>\n                            <ion-label class=\"form-input\">\n                                Piso :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"piso.errors && piso.dirty && piso.touched\"\n                                          (click)=\"mostrarToast('Ingresa calle principal entre 1 y 3 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"1\"\n                                    inputmode=\"numeric\"\n                                    minlength=\"1\"\n                                    maxlength=\"3\"\n                                    name=\"piso\"\n                                    #piso=\"ngModel\"\n                                    [(ngModel)]=\"direccionACrear.piso\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--                referencia-->\n                    <ion-row>\n                        <ion-col>\n                            <ion-label class=\"form-input\">\n                               * Referencia :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"referencia.errors && referencia.dirty && referencia.touched\"\n                                          (click)=\"mostrarToast('Ingresa calle principal entre 4 y 100 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-textarea\n                                    rows=\"6\"\n                                    cols=\"20\"\n                                    required\n                                    placeholder=\"Frente a la gasolinera\"\n                                    minlength=\"4\"\n                                    maxlength=\"255\"\n                                    name=\"referencia\"\n                                    #referencia=\"ngModel\"\n                                    [(ngModel)]=\"direccionACrear.referencia\"\n                            ></ion-textarea>\n                        </ion-col>\n                    </ion-row>\n\n                </ion-grid>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-button\n                        expand=\"block\"\n                        color=\"success\"\n                        [disabled]=\"callePrincipal.errors || calleSecundaria.errors\n                        || numeroCalle.errors || codigoPostal.errors\n                        || nombreEdificio.errors || piso.errors || referencia.errors\"\n                        (click)=\"crearDireccion()\">\n                    {{direccion ? 'Edita' : 'Crea'}} la dirección\n                    <ion-icon class=\"icono-boton\" name=\"mail-unread\"></ion-icon>\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.html":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.html ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"warning\">\n        <ion-title class=\"ion-text-center\">\n            <img class=\"icono-arriba icono-arriba-espacio-modal\" src=\"assets/imagenes/logo-alitas-cabecera.svg\" alt=\"logo\">\n        </ion-title>\n        <ion-buttons slot=\"primary\">\n            <ion-button (click)=\"dismiss()\">\n                <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"4\">\n                <img src=\"assets/imagenes/direccion.png\" style=\"max-height: 92px;\">\n\n            </ion-col>\n            <ion-col>\n                <h1 text-center class=\"titulo\">Direcciones</h1>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"10\">\n                <p class=\"ion-text-justify texto-descripcion\">\n                    Gestiona las\n                    <span class=\"bold\">\n                        direcciones\n                    </span>\n                    , para poder llegar rápidamente con tu pedido.\n                </p>\n            </ion-col>\n            <ion-col size=\"2\">\n                <ion-button expand=\"block\" color=\"primary\" (click)=\"mostraModalCrearDireccion()\">\n                    +\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <ion-grid>\n        <ion-row>\n            <h3>Direcciones registradas</h3>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-list>\n                    <ion-item *ngFor=\"let direccion of direcciones\">\n                        <app-mostrar-direccion\n                                class=\"todo-ancho\"\n                                [direccion]=\"direccion\"\n                                [mostrarBotonHabilitado]=\"true\"\n                                (emitirValorActualDeBotonHabilitado)=\"desactivarDireccion($event, direccion)\"\n                                (emitirValorActualDeActualizar)=\"mostraModalActualizarDireccion($event)\"\n                        ></app-mostrar-direccion>\n                    </ion-item>\n                </ion-list>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"warning\">\n        <ion-title class=\"ion-text-center\">\n            <img class=\"icono-arriba\" src=\"assets/imagenes/logo-alitas-cabecera.svg\" alt=\"logo\">\n        </ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid class=\"espacio-titulo\">\n        <ion-row>\n            <ion-col size=\"4\">\n                <img class=\"imagen-modal\" src=\"assets/imagenes/perfildeusaurio.png\">\n            </ion-col>\n            <ion-col>\n                <h2 class=\"ion-text-center titulo-modal\">\n                    Perfil usuario\n                </h2>\n                <p class=\"ion-text-center parrafo-modal\">\n                <span class=\"bold\">\n                    Actualiza los datos de tu cuenta como\n                </span>\n                    : Nombres, Apellidos, Correo, Dirección/Sector y/o Celular.\n                </p>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <div class=\"espaciado\"></div>\n                <div class=\"espaciado-pequeno\"></div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"1\"></ion-col>\n            <ion-col>\n                <ion-grid>\n                    <!--        nombres-->\n                    <ion-row>\n                        <ion-col class=\"\">\n                            <ion-label class=\"form-input\">\n                                *Nombres :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"nombres?.errors && nombres?.dirty && nombres?.touched\"\n                                          (click)=\"mostrarToast('Ingresa tus nombres de 3 a 60 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"Luis Andres\"\n                                    required\n                                    minlength=\"3\"\n                                    maxlength=\"60\"\n                                    name=\"nombres\"\n                                    #nombres=\"ngModel\"\n                                    [(ngModel)]=\"nuevoUsuario.nombres\"\n                            ></ion-input>\n\n                        </ion-col>\n                    </ion-row>\n                    <!--        apellidos-->\n                    <ion-row>\n                        <ion-col class=\"\">\n                            <ion-label class=\"form-input\">\n                                *Apellidos :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"apellidos?.errors && apellidos?.dirty && apellidos?.touched\"\n                                          (click)=\"mostrarToast('Ingresa tus apellidos de 3 a 60 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"Caicedo Ayala\"\n                                    required\n                                    minlength=\"3\"\n                                    maxlength=\"60\"\n                                    name=\"apellidos\"\n                                    #apellidos=\"ngModel\"\n                                    [(ngModel)]=\"nuevoUsuario.apellidos\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--        celular-->\n                    <ion-row>\n                        <ion-col class=\"\">\n                            <ion-label class=\"form-input\">\n                                *Celular :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"celular?.errors && celular?.dirty && celular?.touched\"\n                                          (click)=\"mostrarToast('Ingresa solo números.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"097654214\"\n                                    required\n                                    maxlength=\"10\"\n                                    pattern=\"[0-9]*\"\n                                    inputmode=\"numeric\"\n                                    name=\"celular\"\n                                    #celular=\"ngModel\"\n                                    [(ngModel)]=\"nuevoUsuario.celular\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--        direccion-->\n                    <ion-row>\n                        <ion-col class=\"\">\n                            <ion-label class=\"form-input\">\n                                *Dirección :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"direccion?.errors && direccion?.dirty && direccion?.touched\"\n                                          (click)=\"mostrarToast('Ingresa tu dirección de 3 a 60 caracteres.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"La gasca\"\n                                    required\n                                    minlength=\"3\"\n                                    maxlength=\"60\"\n                                    name=\"direccion\"\n                                    #direccion=\"ngModel\"\n                                    [(ngModel)]=\"nuevoUsuario.direccion\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                    <!--        identificacion-->\n                    <ion-row>\n                        <ion-col class=\"\">\n                            <ion-label class=\"form-input\">\n                                *Identificación :\n                                <ion-chip color=\"danger\" class=\"chip-error\"\n                                          *ngIf=\"identificacionPais?.errors && identificacionPais?.dirty && identificacionPais?.touched\"\n                                          (click)=\"mostrarToast('Ingresa tu identificación de país.')\">\n                                    <ion-label color=\"danger\">\n                                        <ion-icon name=\"close\"></ion-icon>\n                                    </ion-label>\n                                </ion-chip>\n                            </ion-label>\n                            <ion-input\n                                    placeholder=\"1715123347\"\n                                    required\n                                    [disabled]=\"true\"\n                                    minlength=\"3\"\n                                    maxlength=\"60\"\n                                    name=\"identificacionPais\"\n                                    #identificacionPais=\"ngModel\"\n                                    [(ngModel)]=\"nuevoUsuario.identificacionPais\"\n                            ></ion-input>\n                        </ion-col>\n                    </ion-row>\n                </ion-grid>\n            </ion-col>\n            <ion-col size=\"1\"></ion-col>\n        </ion-row>\n\n        <ion-row>\n            <ion-col>\n                <ion-button class=\"\" expand=\"block\" color=\"success\"\n                            [disabled]=\"nombres.errors || apellidos.errors\"\n                            (click)=\"actualizar()\"\n                >\n                    Actualizar datos\n                    <ion-icon class=\"icono-boton\" name=\"done-all\"></ion-icon>\n                </ion-button>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <div class=\"espaciado-pequeno\"></div>\n                <div class=\"espaciado\"></div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <p class=\"ion-text-center parrafo-modal\">\n                    También puedes cambiar la <span class=\"bold\">dirección de correo electrónico</span> para poder\n                    <span class=\"bold\">restablecer tu contraseña</span>. Y no olvides de agregar direcciones de\n                    domicilio que van a ser utilizadas al momento de crear <span class=\"bold\">pedidos</span>.\n                </p>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-button expand=\"block\" color=\"primary\" (click)=\"mostrarModalGestionDirecciones()\">\n                    Direcciones\n                    <ion-icon class=\"icono-boton\" name=\"paper\"></ion-icon>\n                </ion-button>\n            </ion-col>\n            <ion-col>\n                <ion-button expand=\"block\" color=\"primary\" (click)=\"mostrarModalCambiarCorreo()\">\n                    Cambiar correo electrónico\n                    <ion-icon name=\"mail\"></ion-icon>\n                </ion-button>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <div class=\"espaciado\"></div>\n                <div class=\"espaciado-pequeno\"></div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-button expand=\"block\" color=\"danger\" (click)=\"cerrraSesion()\">\n                    Cerrar sesión\n                    <ion-icon name=\"close\"></ion-icon>\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.scss":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.scss ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".rosado {\n  color: #CC0061;\n}\n\n.marcador {\n  max-width: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2Rldi0wNy9EZXNrdG9wL3RyYW5zcG9ydGVzLW1vdmlsL3RyYW5zcG9ydGVzLWFwcC9zcmMvYXBwL3N1Ym1vZHVsb3Mvc3VibW9kdWxvLWF1dGgwLW1vdmlsL21vZGFsZXMvbW9kYWwtY3JlYXItZGlyZWNjaW9uL21vZGFsLWNyZWFyLWRpcmVjY2lvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc3VibW9kdWxvcy9zdWJtb2R1bG8tYXV0aDAtbW92aWwvbW9kYWxlcy9tb2RhbC1jcmVhci1kaXJlY2Npb24vbW9kYWwtY3JlYXItZGlyZWNjaW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtBQ0NGOztBRENBO0VBQ0UsZUFBQTtBQ0VGIiwiZmlsZSI6InNyYy9hcHAvc3VibW9kdWxvcy9zdWJtb2R1bG8tYXV0aDAtbW92aWwvbW9kYWxlcy9tb2RhbC1jcmVhci1kaXJlY2Npb24vbW9kYWwtY3JlYXItZGlyZWNjaW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJvc2FkbyB7XG4gIGNvbG9yOiAjQ0MwMDYxO1xufVxuLm1hcmNhZG9ye1xuICBtYXgtd2lkdGg6IDEzcHg7XG59XG4iLCIucm9zYWRvIHtcbiAgY29sb3I6ICNDQzAwNjE7XG59XG5cbi5tYXJjYWRvciB7XG4gIG1heC13aWR0aDogMTNweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.ts":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: ModalCrearDireccionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCrearDireccionComponent", function() { return ModalCrearDireccionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _servicios_rest_direccion_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../servicios/rest/direccion-rest.service */ "./src/app/servicios/rest/direccion-rest.service.ts");
/* harmony import */ var _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../servicios/toaster-service/toaster.service */ "./src/app/servicios/toaster-service/toaster.service.ts");
/* harmony import */ var _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../servicios/cargando-service/cargando.service */ "./src/app/servicios/cargando-service/cargando.service.ts");
/* harmony import */ var _constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../constantes/mensajes/mensaje-error */ "./src/app/constantes/mensajes/mensaje-error.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_rest_localizacion_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../servicios/rest/localizacion.service */ "./src/app/servicios/rest/localizacion.service.ts");








let ModalCrearDireccionComponent = class ModalCrearDireccionComponent {
    constructor(_direccionRestService, _toastService, _cargandoService, _localizacionService, modalController) {
        this._direccionRestService = _direccionRestService;
        this._toastService = _toastService;
        this._cargandoService = _cargandoService;
        this._localizacionService = _localizacionService;
        this.modalController = modalController;
        this.direccionACrear = {};
        this.aceptoUbicacion = false;
    }
    ngOnInit() {
        console.log(this.direccion);
        if (this.direccion) {
            this.direccionACrear = Object.assign({}, this.direccion);
        }
    }
    dismiss() {
        this.modalController.dismiss(undefined);
    }
    crearDireccion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.direccionACrear.datosUsuario = this.idUsuario;
            this._cargandoService.habilitarCargandoService('Creando dirección...');
            try {
                let direccion;
                if (this.direccion) {
                    delete this.direccionACrear.id;
                    delete this.direccionACrear.datosUsuario;
                    delete this.direccionACrear.createdAt;
                    delete this.direccionACrear.updatedAt;
                    direccion = yield this._direccionRestService
                        .update(this.direccion.id, this.direccionACrear);
                    const localizacionEncontrada = yield this._localizacionService
                        .buscarLocalizacion({
                        entidadId: this.direccion.id,
                        entidadNombre: 'direccion'
                    });
                    const localizacion = yield this._localizacionService
                        .actualizarLocalizacion(localizacionEncontrada.id, {
                        localizacion: {
                            entidadId: direccion.id.toString(),
                            entidadNombre: 'direccion',
                            localizacion: {
                                type: 'Point',
                                coordinates: this.ubicacionSeleccionada
                            }
                        }
                    });
                }
                else {
                    direccion = yield this._direccionRestService
                        .create(this.direccionACrear);
                    yield this._localizacionService
                        .crearLocalizacion({
                        localizacion: {
                            entidadId: direccion.id.toString(),
                            entidadNombre: 'direccion',
                            localizacion: {
                                type: 'Point',
                                coordinates: this.ubicacionSeleccionada
                            }
                        }
                    });
                }
                this._cargandoService.deshabilitarCargandoService();
                this._toastService.mostrarToast('Se creo correctamente', 'success');
                if (this.direccion) {
                    this.modalController.dismiss({
                        actualizado: true, registro: Object.assign({}, this.direccion, direccion)
                    });
                }
                else {
                    this.modalController.dismiss(direccion);
                }
            }
            catch (error) {
                console.log({
                    error,
                    mensaje: 'Error creando o editando direccion',
                });
                this._cargandoService.deshabilitarCargandoService();
                this._toastService.mostrarToast(_constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_5__["MENSAJE_ERROR"]);
            }
        });
    }
    mostrarToast(mensaje) {
        this._toastService.mostrarToast(mensaje);
    }
    seleccionoPunto(respuesta) {
        this.ubicacionSeleccionada = respuesta.coordenada;
        console.log(respuesta);
    }
    aceptarUbicacion() {
        if (this.ubicacionSeleccionada) {
            this.aceptoUbicacion = true;
        }
        else {
            this.aceptoUbicacion = false;
        }
    }
};
ModalCrearDireccionComponent.ctorParameters = () => [
    { type: _servicios_rest_direccion_rest_service__WEBPACK_IMPORTED_MODULE_2__["DireccionRestService"] },
    { type: _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_3__["ToasterService"] },
    { type: _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_4__["CargandoService"] },
    { type: _servicios_rest_localizacion_service__WEBPACK_IMPORTED_MODULE_7__["LocalizacionService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], ModalCrearDireccionComponent.prototype, "idUsuario", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], ModalCrearDireccionComponent.prototype, "direccion", void 0);
ModalCrearDireccionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-crear-direccion',
        template: __webpack_require__(/*! raw-loader!./modal-crear-direccion.component.html */ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.html"),
        styles: [__webpack_require__(/*! ./modal-crear-direccion.component.scss */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_servicios_rest_direccion_rest_service__WEBPACK_IMPORTED_MODULE_2__["DireccionRestService"],
        _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_3__["ToasterService"],
        _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_4__["CargandoService"],
        _servicios_rest_localizacion_service__WEBPACK_IMPORTED_MODULE_7__["LocalizacionService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]])
], ModalCrearDireccionComponent);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.module.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.module.ts ***!
  \****************************************************************************************************************/
/*! exports provided: ModalCrearDireccionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCrearDireccionModule", function() { return ModalCrearDireccionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modal_crear_direccion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal-crear-direccion.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");






let ModalCrearDireccionModule = class ModalCrearDireccionModule {
};
ModalCrearDireccionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _modal_crear_direccion_component__WEBPACK_IMPORTED_MODULE_3__["ModalCrearDireccionComponent"],
        ],
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
        ],
        entryComponents: [
            _modal_crear_direccion_component__WEBPACK_IMPORTED_MODULE_3__["ModalCrearDireccionComponent"],
        ]
    })
], ModalCrearDireccionModule);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.scss":
/*!*****************************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.scss ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".todo-ancho {\n  width: 100%;\n}\n\n.texto-descripcion {\n  margin: 3px;\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2Rldi0wNy9EZXNrdG9wL3RyYW5zcG9ydGVzLW1vdmlsL3RyYW5zcG9ydGVzLWFwcC9zcmMvYXBwL3N1Ym1vZHVsb3Mvc3VibW9kdWxvLWF1dGgwLW1vdmlsL21vZGFsZXMvbW9kYWwtZ2VzdGlvbi1kaXJlY2Npb25lcy9tb2RhbC1nZXN0aW9uLWRpcmVjY2lvbmVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zdWJtb2R1bG9zL3N1Ym1vZHVsby1hdXRoMC1tb3ZpbC9tb2RhbGVzL21vZGFsLWdlc3Rpb24tZGlyZWNjaW9uZXMvbW9kYWwtZ2VzdGlvbi1kaXJlY2Npb25lcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7QUNDRjs7QURDQTtFQUNFLFdBQUE7RUFDQSxVQUFBO0FDRUYiLCJmaWxlIjoic3JjL2FwcC9zdWJtb2R1bG9zL3N1Ym1vZHVsby1hdXRoMC1tb3ZpbC9tb2RhbGVzL21vZGFsLWdlc3Rpb24tZGlyZWNjaW9uZXMvbW9kYWwtZ2VzdGlvbi1kaXJlY2Npb25lcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b2RvLWFuY2hve1xuICB3aWR0aDogMTAwJTtcbn1cbi50ZXh0by1kZXNjcmlwY2lvbntcbiAgbWFyZ2luOiAzcHg7XG4gIHBhZGRpbmc6IDA7XG59XG4iLCIudG9kby1hbmNobyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4udGV4dG8tZGVzY3JpcGNpb24ge1xuICBtYXJnaW46IDNweDtcbiAgcGFkZGluZzogMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.ts":
/*!***************************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.ts ***!
  \***************************************************************************************************************************/
/*! exports provided: ModalGestionDireccionesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalGestionDireccionesComponent", function() { return ModalGestionDireccionesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../servicios/auth0/auth0.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_rest_direccion_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../servicios/rest/direccion-rest.service */ "./src/app/servicios/rest/direccion-rest.service.ts");
/* harmony import */ var _modal_crear_direccion_modal_crear_direccion_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modal-crear-direccion/modal-crear-direccion.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.component.ts");
/* harmony import */ var _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../servicios/cargando-service/cargando.service */ "./src/app/servicios/cargando-service/cargando.service.ts");
/* harmony import */ var _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../servicios/toaster-service/toaster.service */ "./src/app/servicios/toaster-service/toaster.service.ts");
/* harmony import */ var _constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../constantes/mensajes/mensaje-error */ "./src/app/constantes/mensajes/mensaje-error.ts");









let ModalGestionDireccionesComponent = class ModalGestionDireccionesComponent {
    constructor(modalController, _auth0Service, _cargandoService, _toastService, _direccionRestService) {
        this.modalController = modalController;
        this._auth0Service = _auth0Service;
        this._cargandoService = _cargandoService;
        this._toastService = _toastService;
        this._direccionRestService = _direccionRestService;
    }
    dismiss() {
        this.modalController.dismiss(undefined);
    }
    ngOnInit() {
        this.cargarDirecciones();
    }
    cargarDirecciones() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const consulta = {
                where: {
                    datosUsuario: this._auth0Service.usuario.id
                },
                order: {
                    id: 'DESC'
                }
            };
            const resultadoDirecciones = yield this._direccionRestService
                .findAll(consulta);
            this.direcciones = resultadoDirecciones[0];
        });
    }
    mostraModalCrearDireccion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modal_crear_direccion_modal_crear_direccion_component__WEBPACK_IMPORTED_MODULE_5__["ModalCrearDireccionComponent"],
                componentProps: {
                    idUsuario: this._auth0Service.usuario.id
                }
            });
            yield modal.present();
            const resultadoModal = yield modal.onWillDismiss();
            if (resultadoModal.data) {
                this.direcciones.unshift(resultadoModal.data);
            }
        });
    }
    mostraModalActualizarDireccion(direccion) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modal_crear_direccion_modal_crear_direccion_component__WEBPACK_IMPORTED_MODULE_5__["ModalCrearDireccionComponent"],
                componentProps: {
                    idUsuario: this._auth0Service.usuario.id,
                    direccion
                }
            });
            yield modal.present();
            const resultadoModal = yield modal.onWillDismiss();
            if (resultadoModal.data) {
                const direccionEditada = resultadoModal.data.registro;
                const indice = this.direcciones.findIndex((a) => a.id === direccion.id);
                this.direcciones[indice] = direccionEditada;
            }
        });
    }
    desactivarDireccion(habilitado, direccion) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            habilitado = habilitado ? 0 : 1;
            const direccionAActualizar = {
                habilitado
            };
            yield this._cargandoService.habilitarCargandoService('Desactivando direccion');
            try {
                const direccionActualizada = yield this._direccionRestService
                    .update(direccion.id, direccionAActualizar);
                console.log('direccion actualizada', direccionActualizada);
                yield this._cargandoService.deshabilitarCargandoService();
                const mensaje = habilitado ? 'Se activo correctamente' : 'Se desactivo correctamente';
                yield this._toastService.mostrarToast(mensaje, 'success');
                direccion.habilitado = habilitado;
            }
            catch (e) {
                console.log(e);
                yield this._cargandoService.deshabilitarCargandoService();
                yield this._toastService.mostrarToast(_constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_8__["MENSAJE_ERROR"]);
            }
        });
    }
};
ModalGestionDireccionesComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__["Auth0Service"] },
    { type: _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_6__["CargandoService"] },
    { type: _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_7__["ToasterService"] },
    { type: _servicios_rest_direccion_rest_service__WEBPACK_IMPORTED_MODULE_4__["DireccionRestService"] }
];
ModalGestionDireccionesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-gestion-direcciones',
        template: __webpack_require__(/*! raw-loader!./modal-gestion-direcciones.component.html */ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.html"),
        styles: [__webpack_require__(/*! ./modal-gestion-direcciones.component.scss */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__["Auth0Service"],
        _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_6__["CargandoService"],
        _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_7__["ToasterService"],
        _servicios_rest_direccion_rest_service__WEBPACK_IMPORTED_MODULE_4__["DireccionRestService"]])
], ModalGestionDireccionesComponent);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.module.ts":
/*!************************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.module.ts ***!
  \************************************************************************************************************************/
/*! exports provided: ModalGestionDireccionesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalGestionDireccionesModule", function() { return ModalGestionDireccionesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _modal_gestion_direcciones_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-gestion-direcciones.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modal_crear_direccion_modal_crear_direccion_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../modal-crear-direccion/modal-crear-direccion.module */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-crear-direccion/modal-crear-direccion.module.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _componentes_mostrar_direccion_mostrar_direccion_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../componentes/mostrar-direccion/mostrar-direccion.module */ "./src/app/submodulos/submodulo-auth0-movil/componentes/mostrar-direccion/mostrar-direccion.module.ts");







let ModalGestionDireccionesModule = class ModalGestionDireccionesModule {
};
ModalGestionDireccionesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _modal_gestion_direcciones_component__WEBPACK_IMPORTED_MODULE_2__["ModalGestionDireccionesComponent"],
        ],
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _modal_crear_direccion_modal_crear_direccion_module__WEBPACK_IMPORTED_MODULE_4__["ModalCrearDireccionModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"],
            _componentes_mostrar_direccion_mostrar_direccion_module__WEBPACK_IMPORTED_MODULE_6__["MostrarDireccionModule"]
        ],
        entryComponents: [
            _modal_gestion_direcciones_component__WEBPACK_IMPORTED_MODULE_2__["ModalGestionDireccionesComponent"],
        ]
    })
], ModalGestionDireccionesModule);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.module.ts ***!
  \********************************************************************************/
/*! exports provided: PerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./perfil.page */ "./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.ts");
/* harmony import */ var _servicios_servicios_auth0_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../servicios/servicios-auth0.module */ "./src/app/submodulos/submodulo-auth0-movil/servicios/servicios-auth0.module.ts");
/* harmony import */ var _modales_modal_gestion_direcciones_modal_gestion_direcciones_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../modales/modal-gestion-direcciones/modal-gestion-direcciones.module */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.module.ts");
/* harmony import */ var _modales_modal_cambiar_correo_modal_cambiar_correo_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modales/modal-cambiar-correo/modal-cambiar-correo.module */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-cambiar-correo/modal-cambiar-correo.module.ts");










const routes = [
    {
        path: '',
        component: _perfil_page__WEBPACK_IMPORTED_MODULE_6__["PerfilPage"]
    }
];
let PerfilPageModule = class PerfilPageModule {
};
PerfilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _servicios_servicios_auth0_module__WEBPACK_IMPORTED_MODULE_7__["ServiciosAuth0Module"],
            _modales_modal_gestion_direcciones_modal_gestion_direcciones_module__WEBPACK_IMPORTED_MODULE_8__["ModalGestionDireccionesModule"],
            _modales_modal_cambiar_correo_modal_cambiar_correo_module__WEBPACK_IMPORTED_MODULE_9__["ModalCambiarCorreoModule"],
        ],
        declarations: [_perfil_page__WEBPACK_IMPORTED_MODULE_6__["PerfilPage"]]
    })
], PerfilPageModule);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.scss":
/*!********************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1Ym1vZHVsb3Mvc3VibW9kdWxvLWF1dGgwLW1vdmlsL3J1dGFzL3BlcmZpbC9wZXJmaWwucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.ts":
/*!******************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.ts ***!
  \******************************************************************************/
/*! exports provided: PerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPage", function() { return PerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../servicios/auth0/auth0.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service.ts");
/* harmony import */ var _servicios_rest_datos_usuario_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../servicios/rest/datos-usuario-rest.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/rest/datos-usuario-rest.service.ts");
/* harmony import */ var _servicios_local_storage_local_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../servicios/local-storage/local-storage.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/local-storage/local-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../constantes/mensajes/mensaje-error */ "./src/app/constantes/mensajes/mensaje-error.ts");
/* harmony import */ var _modales_modal_gestion_direcciones_modal_gestion_direcciones_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../modales/modal-gestion-direcciones/modal-gestion-direcciones.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-gestion-direcciones/modal-gestion-direcciones.component.ts");
/* harmony import */ var _modales_modal_cambiar_correo_modal_cambiar_correo_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modales/modal-cambiar-correo/modal-cambiar-correo.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-cambiar-correo/modal-cambiar-correo.component.ts");
/* harmony import */ var _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../servicios/cargando-service/cargando.service */ "./src/app/servicios/cargando-service/cargando.service.ts");











let PerfilPage = class PerfilPage {
    constructor(toastController, auth0Service, alertController, _datosUsuarioRestService, _localStorageService, _router, modalController, _cargandoService) {
        this.toastController = toastController;
        this.auth0Service = auth0Service;
        this.alertController = alertController;
        this._datosUsuarioRestService = _datosUsuarioRestService;
        this._localStorageService = _localStorageService;
        this._router = _router;
        this.modalController = modalController;
        this._cargandoService = _cargandoService;
        this.nuevoUsuario = {};
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.inicializarDatosUsuario();
    }
    inicializarDatosUsuario() {
        this.nuevoUsuario.nombres = this.auth0Service.usuario.nombres;
        this.nuevoUsuario.apellidos = this.auth0Service.usuario.apellidos;
        this.nuevoUsuario.direccion = this.auth0Service.usuario.direccion;
        this.nuevoUsuario.identificacionPais = this.auth0Service.usuario.identificacionPais;
        this.nuevoUsuario.celular = this.auth0Service.usuario.celular;
    }
    mostrarToast(texto, color = 'danger', tiempo = 3000) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: texto,
                duration: tiempo,
                color
            });
            toast.present();
        });
    }
    actualizar() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this._cargandoService.habilitarCargandoService('Actualizando datos...');
            try {
                const idUsuario = this.auth0Service.usuario.id;
                const usuarioActualizado = yield this._datosUsuarioRestService.update(idUsuario, this.nuevoUsuario);
                this.auth0Service.usuario.nombres = this.nuevoUsuario.nombres;
                this.auth0Service.usuario.apellidos = this.nuevoUsuario.apellidos;
                this._localStorageService.setItem('usuario', this.auth0Service.usuario);
                const usuarioAc = yield this._localStorageService.getItem('usuario');
                yield this._cargandoService.deshabilitarCargandoService();
                this.mostrarToast('Usuario actualizado.', 'success');
            }
            catch (e) {
                console.error({
                    mensaje: 'Error actualizando usuario',
                    error: e
                });
                yield this._cargandoService.deshabilitarCargandoService();
                this.mostrarToast(_constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_7__["MENSAJE_ERROR"]);
            }
        });
    }
    cerrraSesion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Confirmar',
                message: '¿Deseas cerrar sesión?',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                        }
                    }, {
                        text: 'Aceptar',
                        cssClass: 'success',
                        handler: () => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            try {
                                yield this.auth0Service.logout();
                                const url = ['/tabs', 'login'];
                                yield this._router.navigate(url);
                            }
                            catch (e) {
                                console.error({ error: e, mensaje: 'Error cerrar sesion' });
                            }
                        })
                    }
                ]
            });
            yield alert.present();
        });
    }
    mostrarModalGestionDirecciones() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modales_modal_gestion_direcciones_modal_gestion_direcciones_component__WEBPACK_IMPORTED_MODULE_8__["ModalGestionDireccionesComponent"]
            });
            return yield modal.present();
        });
    }
    mostrarModalCambiarCorreo() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modales_modal_cambiar_correo_modal_cambiar_correo_component__WEBPACK_IMPORTED_MODULE_9__["ModalCambiarCorreoComponent"]
            });
            return yield modal.present();
        });
    }
};
PerfilPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_3__["Auth0Service"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _servicios_rest_datos_usuario_rest_service__WEBPACK_IMPORTED_MODULE_4__["DatosUsuarioRestService"] },
    { type: _servicios_local_storage_local_storage_service__WEBPACK_IMPORTED_MODULE_5__["LocalStorageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_10__["CargandoService"] }
];
PerfilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-perfil',
        template: __webpack_require__(/*! raw-loader!./perfil.page.html */ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.html"),
        styles: [__webpack_require__(/*! ./perfil.page.scss */ "./src/app/submodulos/submodulo-auth0-movil/rutas/perfil/perfil.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_3__["Auth0Service"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _servicios_rest_datos_usuario_rest_service__WEBPACK_IMPORTED_MODULE_4__["DatosUsuarioRestService"],
        _servicios_local_storage_local_storage_service__WEBPACK_IMPORTED_MODULE_5__["LocalStorageService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_10__["CargandoService"]])
], PerfilPage);



/***/ })

}]);
//# sourceMappingURL=submodulos-submodulo-auth0-movil-rutas-perfil-perfil-module-es2015.js.map