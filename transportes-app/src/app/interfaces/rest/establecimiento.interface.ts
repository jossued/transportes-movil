import {EdificioInterface} from './edificio.interface';
import {HorarioEstablecimientoInterface} from './horario-establecimiento.interface';

export interface EstablecimientoInterface {
    id?: number;
    createdAt?: string;
    updatedAt?: string;
    nombre?: string;
    horasRestaAtencion?: number;
    habilitado?: number | boolean;
    valorDelivery: string | number;
    codigo?: string;
    soloDelivery?: 0 | 1;
    edificio?: EdificioInterface | number | string;
    horariosEstablecimiento?: HorarioEstablecimientoInterface[];
}

