import { HorarioInterface } from './horario.interface';
import { SlideMenuInterface } from './slide-menu.interface';
export interface HorarioSlideMenuInterface {
  habilitado?: 0 | 1;
  horario?: HorarioInterface;
  slideMenu?: SlideMenuInterface;
}
