
export interface FindWhereRelations {
    entidad: string;
    key: string;
    query?: {
        campo: string;
        valor: string;
        like?: boolean;
    }[];
}

