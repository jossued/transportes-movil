import {Component, Input, OnInit} from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {EstablecimientoInterface} from '../../interfaces/rest/establecimiento.interface';
import {LocalizacionService} from '../../servicios/rest/localizacion.service';
import {EdificioInterface} from '../../interfaces/rest/edificio.interface';

@Component({
    selector: 'app-modal-info-local',
    templateUrl: './modal-info-local.component.html',
    styleUrls: ['./modal-info-local.component.scss'],
})
export class ModalInfoLocalComponent implements OnInit {

    @Input() local: EstablecimientoInterface;

    coordenadas = {
        latitud: 0,
        longitud: 0
    };

    constructor(public toastController: ToastController,
                public modalController: ModalController,
                private readonly _localizacionService: LocalizacionService,
    ) {
    }

    dismiss() {
        this.modalController.dismiss(undefined);
    }

    async ngOnInit() {
        console.log('HIJUE PUTAS');
        const edificio = this.local.edificio as EdificioInterface;
        const localizacion = await this._localizacionService
            .buscarLocalizacion(
                {
                    entidadId: edificio.id,
                    entidadNombre: 'edificio'
                }
            ) as any;
        const coordenadas = localizacion.localizacion.coordinates;
        this.coordenadas.latitud = coordenadas[0];
        this.coordenadas.longitud = coordenadas[1];
        console.log('coordenadas', coordenadas);
        console.log('local', this.local);
    }
}
