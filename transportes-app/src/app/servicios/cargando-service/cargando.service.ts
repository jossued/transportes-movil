import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable()
export class CargandoService {
    cargando;

    constructor(public loadingController: LoadingController) {
    }

    async habilitarCargandoService(mensaje?: string) {
        this.cargando = await this.loadingController.create({
            message: mensaje ? mensaje : 'Espere un momento...',
            duration: 0,
        });
        return await this.cargando.present();
    }

    async deshabilitarCargandoService() {
        if (this.cargando) {
            return await this.cargando.dismiss();
        } else {
            return null;
        }

    }
}
