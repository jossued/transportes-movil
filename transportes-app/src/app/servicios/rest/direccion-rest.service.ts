import {Injectable} from '@angular/core';
import {IonicRestPrincipalService} from './principal-rest.service';
import {HTTP} from '@ionic-native/http/ngx';
import {environment} from '../../../environments/environment';
import {DireccionInterface} from '../../interfaces/rest/direccion.interface';

@Injectable()
export class DireccionRestService extends IonicRestPrincipalService<DireccionInterface> {
    constructor(http: HTTP) {
        super(environment.url, '/direccion', http, {});
    }
}