import {EstablecimientoInterface} from '../../interfaces/rest/establecimiento.interface';
import {environment} from '../../../environments/environment';
import {HTTP} from '@ionic-native/http/ngx';
import {IonicRestPrincipalService} from './principal-rest.service';
import {Injectable} from '@angular/core';
import {CoordenadasInterface} from '../../interfaces/coordenadas.interface';
import {CabecerasIonic} from '../../interfaces/rest/cabecera-ionic-interface';

@Injectable()
export class EstablecimientoRestService extends IonicRestPrincipalService<EstablecimientoInterface> {
    constructor(http: HTTP) {
        super(environment.url, '/establecimiento', http, {});
    }

    encontrarEstablecimientosPorCoordenada(coordenadas: CoordenadasInterface, cabeceras: CabecerasIonic = {}) {
        const path = `encontrarEstablecimientosPorCoordenada`;
        const url = `${environment.url}/establecimiento/${path}?latitud=${coordenadas.latidud}&&longitud=${coordenadas.longitud}`;
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.get(url, undefined, headers);
                    const respuesta = request.data as { registros: number };
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }


}
