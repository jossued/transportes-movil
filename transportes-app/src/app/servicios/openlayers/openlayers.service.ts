import {EventEmitter, Injectable} from '@angular/core';
import {InicializarMapa} from './interfaces/inicializar-mapa';
import {MarcadorImagenOpenLayer} from './interfaces/marcador-imagen-open-layer';
import {ObjetoEventoClickOpenLayer} from './interfaces/objeto-click-open-layer';
import {LineaInicioFinConfiguracion} from './interfaces/linea-inicio-fin-configuracion';
import {DeviceInfo, Plugins} from '@capacitor/core';

const {Geolocation} = Plugins;


// const styleCache = {};
declare var ol;
declare var styleCache;

@Injectable()
export class OpenlayersService {

    mapa;
    view;
    vectorSource;
    positionFeature;
    accuracyFeature;
    geolocation;
    rastrearAutomaticamente = false;
    emitioIrAPosicionActual: EventEmitter<boolean> = new EventEmitter();

    inicializarMapaOpenLayers(configuracion: InicializarMapa, device?: DeviceInfo): any {
        if (!configuracion.zoom) {
            configuracion.zoom = 17;
        }
        let coordenadas = [
            configuracion.longitud,
            configuracion.latitud
        ];
        if (!device) {
            coordenadas = [
                configuracion.longitud,
                configuracion.latitud
            ];
        } else {
            if (device.platform === 'ios') {
                coordenadas = [
                    configuracion.longitud,
                    configuracion.latitud
                ];
            }
            if (device.platform === 'web') {
                coordenadas = [
                    configuracion.longitud,
                    configuracion.latitud
                ];
            }
        }

        this.view = new ol.View({
            center: ol.proj.fromLonLat(
                coordenadas
            ),
            zoom: configuracion.zoom
        });
        const mapa = new ol.Map({
            target: configuracion.nombreMapa,
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: this.view,
        });

        if (configuracion.mostrarEscala) {
            const ctrl = new ol.control.Scale({});
            mapa.addControl(ctrl);
            mapa.addControl(new ol.control.ScaleLine());
        }

        if (configuracion.mostrarPuntoUsuario) {
            this.craerPuntoUsuarioMapa(mapa, coordenadas[1], coordenadas[0], configuracion.imagenPuntoUsuario);
        }

        if (configuracion.mostrarIrAPuntoUsuario) {
            let html;
            let clase;
            let titulo;
            if (configuracion.configuracionBotonIrAPuntoUsuario) {
                html = configuracion.configuracionBotonIrAPuntoUsuario.html;
                clase = configuracion.configuracionBotonIrAPuntoUsuario.claseCss;
                titulo = configuracion.configuracionBotonIrAPuntoUsuario.titulo;
            }

            const botonIrAPuntoUsuario = new ol.control.Button(
                {
                    html: html ? html : '<ion-icon name="locate"></ion-icon>',
                    className: clase ? clase : 'ol-boton-posicion',
                    title: titulo ? titulo : 'Ir a posición actual',
                    handleClick: async () => {
                        try {
                            const coordenadasOriginales = await Geolocation.getCurrentPosition();
                            const coordenadasEnLatitudLongitud = ol.proj
                                .transform(
                                    [
                                        coordenadasOriginales.coords.longitude,
                                        coordenadasOriginales.coords.latitude,
                                    ],
                                    'EPSG:3857',
                                    'EPSG:4326',
                                );
                            console.log({
                                mensaje: `Esta rastreando con la geolocalizacion del navegador:
                            ${this.rastrearAutomaticamente}, dio click para ir a la posicion actual`,
                                coordenadas: `Latitud: ${coordenadasEnLatitudLongitud[1]} Longitud: ${coordenadasEnLatitudLongitud[0]}`
                            });
                            if (this.rastrearAutomaticamente) {
                                this.centrarEnLatitudLongitud(this.mapa, coordenadasEnLatitudLongitud[1], coordenadasEnLatitudLongitud[0]);
                            } else {
                                console.log({
                                    mensaje: 'Use la funcion: this.centrarEnLatitudLongitud(mapa,latitud,longitud)'
                                });
                                this.emitioIrAPosicionActual.emit(true);
                            }
                        } catch (e) {
                            console.error({
                                error: e,
                                mensaje: 'Error cargando coordenadas'
                            });
                        }


                    }
                });
            mapa.addControl(botonIrAPuntoUsuario);
        }

        const numero = configuracion.intervalo as number;
        setInterval(
            () => {
                mapa.updateSize();
            },
            numero
        );

        return mapa;
    }

    cargarPuntosConImagenes(marcadoresImagen: MarcadorImagenOpenLayer[], mapa: any) {
        if (marcadoresImagen.length > 0) {
            const features = marcadoresImagen.map(
                (marcador: MarcadorImagenOpenLayer) => {
                    const objetosDePropiedades = {
                        ...marcador.objetoMarcadorImagen
                    };
                    console.log('Objeto de Propiedades', objetosDePropiedades);
                    return {
                        type: 'Feature',
                        geometry: {
                            type: 'Point',
                            coordinates: ol.proj.transform([marcador.longitud, marcador.latitud], 'EPSG:4326', 'EPSG:3857')
                        },
                        properties: {
                            objetoImagen: {
                                ...objetosDePropiedades
                            }
                        }
                    };
                }
            );
            const vectorSource = new ol.source.Vector({
                loader: (extent, resolution, projection) => {
                    vectorSource.addFeatures(
                        vectorSource.getFormat().readFeatures({
                            type: 'FeatureCollection',
                            features
                        }));

                },
                projection: 'EPSG:3857',
                format: new ol.format.GeoJSON(),
                attributions: ['&copy; <a href=\'https://manticore-labs.com\'>manticore-labs.com</a>'],
                logo: 'https://ideas.manticore-labs.com/wp-content/uploads/2018/08/Logo-sitio.png'
            });

            const vector = new ol.layer.Vector(
                {
                    name: 'imagenes',
                    source: vectorSource,
                    renderOrder: ol.ordering.yOrdering(),
                    style: getFeatureStyle
                });
            mapa.addLayer(vector);
            return mapa;
        } else {
            console.error({
                error: 400,
                mensaje: 'Error, no tiene ningun marcador seleccionado'
            });
            return mapa;
        }

    }

    escucharCambios(mapa: any, callback): () => ObjetoEventoClickOpenLayer<any> {
        const select = new ol.interaction.Select(
            {
                condition: ol.events.condition.click,
                style: (feature, resolution) => {
                    return getFeatureStyle(feature, resolution, true);
                }
            });
        mapa.addInteraction(select);
        select
            .getFeatures()
            .on(['add', 'remove'], (e) => {
                const elemento = e.element;
                const coordenadas = elemento.get('geometry').flatCoordinates;
                const latitudLongitud = ol.proj.transform(coordenadas, 'EPSG:3857', 'EPSG:4326');
                const objetoImagen = elemento.get('objetoImagen');
                if (e.type === 'add') {
                    objetoImagen.focus = true;
                    callback({
                        objetoImagen,
                        coordenadas: {
                            latitud: latitudLongitud[1],
                            longitud: latitudLongitud[0],
                        },
                        elemento,
                    });
                } else {
                    objetoImagen.focus = false;
                    elemento.set('objetoImagen', objetoImagen);
                    callback({
                        objetoImagen,
                        coordenadas: {
                            latitud: latitudLongitud[1],
                            longitud: latitudLongitud[0],
                        },
                        elemento,
                        salioDeFoco: true
                    });
                }
            });
        return mapa;
    }

    dibujarLineaConInicioFin(configuracionLineaInicioFin: LineaInicioFinConfiguracion, mapa: any) {

        const features = [
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: ol.proj.transform([
                        configuracionLineaInicioFin.coordenadaInicial.longitud,
                        configuracionLineaInicioFin.coordenadaInicial.latitud
                    ], 'EPSG:4326', 'EPSG:3857')
                },
                properties: {}
            },
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: ol.proj.transform([
                        configuracionLineaInicioFin.coordenadaFinal.longitud,
                        configuracionLineaInicioFin.coordenadaFinal.latitud
                    ], 'EPSG:4326', 'EPSG:3857')
                },
                properties: {}
            },


        ];
        const vectorSource = new ol.source.Vector({
            loader: (extent, resolution, projection) => {
                vectorSource.addFeatures(
                    vectorSource.getFormat().readFeatures({
                        type: 'FeatureCollection',
                        features
                    }));

            },
            projection: 'EPSG:3857',
            format: new ol.format.GeoJSON(),
            attributions: ['&copy; <a href=\'https://manticore-labs.com\'>manticore-labs.com</a>'],
            logo: 'https://ideas.manticore-labs.com/wp-content/uploads/2018/08/Logo-sitio.png'
        });
        const estiloPuntoInicialFinal = new ol.style.Style({
            image: new ol.style.Circle({
                radius: configuracionLineaInicioFin.configuracionPuntoInicioFin.radio,
                fill: new ol.style.Fill({
                    color: configuracionLineaInicioFin.configuracionPuntoInicioFin.colorLlenado
                }),
                stroke: new ol.style.Stroke({
                    color: configuracionLineaInicioFin.configuracionPuntoInicioFin.colorLinea,
                    width: configuracionLineaInicioFin.configuracionPuntoInicioFin.anchoLinea
                })
            })
        });


        // creciendo el FlowLine con dos colores
        const estiloCreciendoDosColores = new ol.style.FlowLine({
            color: 'red',
            color2: 'green',
            width: 3,
            width2: 25
        });

        // Un solo color
        const estiloLinea = new ol.style.FlowLine({
            visible: false,
            lineCap: 'round',
            color: configuracionLineaInicioFin.configuracionLinea.colorLlenado,
            width: configuracionLineaInicioFin.configuracionLinea.anchoLinea,
            geometry: (f) => {
                if (f.getGeometry().getType() === 'MultiLineString') {
                    return f.getGeometry().getLineString(0);
                } else {
                    return f.getGeometry();
                }
            }
        });

        function obtenerEstiloPorPosicion(feature, res) {
            return [estiloPuntoInicialFinal, estiloLinea];
        }

        const vector = new ol.layer.Vector({
            renderMode: 'image',
            source: vectorSource,
            style: obtenerEstiloPorPosicion
        });


        mapa.addLayer(vector);
        const lineaString = configuracionLineaInicioFin
            .arregloCoordenadas
            .map(
                (coordenadas) => {
                    return ol.proj.transform([coordenadas[1], coordenadas[0]], 'EPSG:4326', 'EPSG:3857');
                }
            );
        vector
            .getSource()
            .addFeature(
                new ol.Feature(
                    new ol.geom.LineString(
                        lineaString
                        // ol.proj.transform([-78.493701, -0.172593], 'EPSG:4326', 'EPSG:3857'),
                        // ol.proj.transform([-78.493601, -0.172510], 'EPSG:4326', 'EPSG:3857'),
                        // ol.proj.transform([-78.493355, -0.174367,], 'EPSG:4326', 'EPSG:3857')
                    )
                )
            );
        return mapa;
    }

    centrarEnLatitudLongitud(mapa, latitud, longitud, zoom?) {
        if (zoom) {
            mapa = this.setearZoom(mapa, zoom);
        }
        mapa
            .getView()
            .setCenter(ol.proj.transform([longitud, latitud], 'EPSG:4326', 'EPSG:3857'));

        this.mapa = mapa;
        return mapa;
    }

    setearZoom(mapa, zoom) {
        mapa
            .getView()
            .setZoom(zoom);
        this.mapa = mapa;
        return mapa;
    }

    craerPuntoUsuarioMapa(mapa, latitud, longitud, imagenPuntoUsuario: any) {

        // Geolocalizacion
        const opcionesGeolocalizacion = {
            trackingOptions: {
                enableHighAccuracy: true
            },
            projection: this.view.getProjection()
        };

        this.geolocation = new ol.Geolocation(opcionesGeolocalizacion);

        // Icono Posicion
        this.positionFeature = new ol.Feature();
        const estilo = {
            image: imagenPuntoUsuario ? imagenPuntoUsuario : new ol.style.Circle({
                radius: 9,
                fill: new ol.style.Fill({
                    color: '#cc0061'
                }),
                stroke: new ol.style.Stroke({
                    color: '#fff',
                    width: 2
                })
            })
        };

        this.positionFeature.setStyle(new ol.style.Style(estilo));


        // Setear la localizacion del punto
        this.moverPuntoUsuario(latitud, longitud);

        // Mostrar en el mapa
        this.vectorSource = new ol.layer.Vector({
            map: mapa,
            source: new ol.source.Vector({
                features: [this.positionFeature]
            })
        });

        // Empezar rastreo
        setTimeout(() => {
            this.geolocation.setTracking(true);
        }, 1);

        this.escucharCambiosEnGeolocalizacion();
    }

    moverPuntoUsuario(latitud, longitud) {
        const coordenadasTransformadas = ol.proj.transform([longitud, latitud], 'EPSG:4326', 'EPSG:3857');
        this.positionFeature.setGeometry(new ol.geom.Point(coordenadasTransformadas));
    }

    escucharCambiosEnGeolocalizacion() {
        this.geolocation.on('error', (error) => {
            console.error({error, mensaje: 'Error cargando geolocalización'});
        });

        this.geolocation.on('change:position', async () => {
            const coordenadas = await Geolocation.getCurrentPosition();
            const coordenadasEnLatitudLongitud = ol.proj.transform(
                [coordenadas.coords.longitude, coordenadas.coords.latitude],
                'EPSG:3857',
                'EPSG:4326'
            );
            console.log({
                mensaje: `Esta rastreando con la geolocalizacion del navegador:
        ${this.rastrearAutomaticamente}, cambio la posicion en el navegador.`,
                coordenadas: `Latitud: ${coordenadasEnLatitudLongitud[1]} Longitud: ${coordenadasEnLatitudLongitud[0]}`,
            });
            if (this.rastrearAutomaticamente) {

                // this.positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
            }
        });
    }

    dibujarPoligonos(mapa, arregloDePoligonos: number[][][]) {

        const featuresArreglo = arregloDePoligonos
            .map(
                (ar, i) =>
                    this.transformarArregloLatitudLongitudAFeaturePoligono(ar, i + 1)
            );
        const objetofeatures = {
            type: 'FeatureCollection',
            features: featuresArreglo

        };
        const arregloFeatures: any[] = new ol.format
            .GeoJSON()
            .readFeatures(
                JSON.stringify(objetofeatures)
            );
        const vectorSource = new ol.source.Vector();
        arregloFeatures
            .forEach(
                (feature) => {
                    vectorSource.addFeature(feature);
                }
            );
        const vectorLayer = new ol.layer.Vector({
            source: vectorSource
        });
        mapa.addLayer(vectorLayer);
        this.mapa = mapa;
        return mapa;
    }


    transformarArregloLatitudLongitudAFeaturePoligono(arreglo: number[][], id: number) {
        return {
            type: 'Feature',
            id: `${id}`,
            properties: {
                id
            },
            geometry: {
                type: 'Polygon',
                coordinates: [
                    arreglo
                        .map(
                            (ar) => this.transformarLatitudLongitudAFormatoOpenLayers(ar)
                        )
                ]
            }
        };
    }

    transformarLatitudLongitudAFormatoOpenLayers(arreglo: number[]): number[] {
        const latitud = arreglo[0];
        const longitud = arreglo[1];
        return ol.proj.transform([longitud, latitud], 'EPSG:4326', 'EPSG:3857');
    }

    transformarFormatoOpenLayersALatitudLongitud(arreglo: [number, number]): [number, number] {
        const arregloLongitudLatitud = ol.proj.transform([arreglo[0], arreglo[1]], 'EPSG:3857', 'EPSG:4326');
        return [arregloLongitudLatitud[1], arregloLongitudLatitud[0]];
    }

    puntoEstaDentroDePoligono(arregloLatitudLongitud: number[], arregloPoligonoLatitudLongitud: number[][]) {
        const x = arregloLatitudLongitud[0], y = arregloLatitudLongitud[1];
        let inside = false;
        for (let i = 0, j = arregloPoligonoLatitudLongitud.length - 1; i < arregloPoligonoLatitudLongitud.length; j = i++) {
            const xi = arregloPoligonoLatitudLongitud[i][0], yi = arregloPoligonoLatitudLongitud[i][1];
            const xj = arregloPoligonoLatitudLongitud[j][0], yj = arregloPoligonoLatitudLongitud[j][1];

            const intersect = ((yi > y) !== (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) {
                inside = !inside;
            }
        }
        return inside;
    }

}

function getFeatureStyle(feature, resolution, sel) {
    // animaciones
    //     if(e.values_.objetoImagen.focus) sexo.animateFeature(e, [
    //         new ol.featureAnimation['Zoom'](
    //             {	speed: Number(0.2),
    //                 duration: Number(1000-1*300),
    //                 side: false,
    //                 revers: false
    //             })
    //     ]);
    // })
    const objeto = feature.get('objetoImagen');
    if (objeto) {
        let estilo = styleCache[objeto.idMarcador];
        const configuracionParaTexto = objeto.configuracionTexto;
        let estiloTexto;
        if (configuracionParaTexto) {
            estiloTexto = {
                font: configuracionParaTexto.estiloPixelesFuenteDelTexto ?
                    configuracionParaTexto.estiloPixelesFuenteDelTexto : '12px sans-serif',
                text: configuracionParaTexto.nombreAMostrar ? configuracionParaTexto.nombreAMostrar : 'No hay texto',
                offsetY: configuracionParaTexto.espaciadoDesdeElCentroHaciaAbajo ?
                    configuracionParaTexto.espaciadoDesdeElCentroHaciaAbajo : '50',
                textBaseline: 'bottom',
                textAlign: configuracionParaTexto.alineacionTexto ? configuracionParaTexto.alineacionTexto : 'center',
                fill: new ol.style.Fill({color: configuracionParaTexto.colorTexto ? configuracionParaTexto.colorTexto : '#171717'}),
                // backgroundFill: new ol.style.Fill({
                //     color: configuracionParaTexto.colorBackground ? configuracionParaTexto.colorBackground : '#0942c2'
                // }),
                overflow: false,
                scale: 1,
                padding: configuracionParaTexto.padding ? configuracionParaTexto.padding : [5, 5, 5, 5]
            };
        }
        if (estilo) {
            if (objeto.focus && !styleCache[objeto.idMarcador].focus) {
                estiloTexto.backgroundFill = new ol.style.Fill({
                    color: configuracionParaTexto.colorBackground ? configuracionParaTexto.colorBackground : '#00091f'
                });
                estiloTexto.fill = new ol.style.Fill({color: '#fff'});
                estiloTexto.font = '16px sans-serif';
                estiloTexto.offsetY = '68';

                styleCache[objeto.idMarcador].setText(new ol.style.Text(estiloTexto));
                styleCache[objeto.idMarcador].getImage().setScale(1.4);
                styleCache[objeto.idMarcador].focus = true;
            } else {
                if (styleCache[objeto.idMarcador].focus === true) {
                    styleCache[objeto.idMarcador].setText(new ol.style.Text(estiloTexto));
                    styleCache[objeto.idMarcador].getImage().setScale(1);
                    estiloTexto.fill = new ol.style.Fill({color: '#171717'});
                    estiloTexto.font = '12px sans-serif';
                    estiloTexto.offsetY = '50';
                    styleCache[objeto.idMarcador].focus = false;
                }
            }
            styleCache[objeto.idMarcador].objeto = objeto;
            return styleCache[objeto.idMarcador];
        } else {
            estiloTexto.fill = new ol.style.Fill({color: '#00091f'});
            styleCache[objeto.idMarcador] = estilo = new ol.style.Style
            ({
                image: new ol.style.Photo({
                    src: objeto.img,
                    radius: 25,
                    shadow: 1,
                    crop: true,
                    kind: 'folio',
                    stroke: new ol.style.Stroke(
                        {
                            width: 2,
                            color: objeto.focus ? 'rgb(43,85,204)' : 'rgb(254,253,255)',
                        })
                }),
                text: new ol.style.Text(estiloTexto)
            });
            styleCache[objeto.idMarcador].objeto = objeto;
            return styleCache[objeto.idMarcador];
        }
    }
}
