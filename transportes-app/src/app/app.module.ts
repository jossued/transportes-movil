import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ModalInfoLocalComponent } from './modales/modal-info-local/modal-info-local.component';
import { ServiciosAuth0Module } from './submodulos/submodulo-auth0-movil/servicios/servicios-auth0.module';
import { HTTP } from '@ionic-native/http/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { SERVICIOS_PRINCIPAL } from './servicios/servicios-principal';
import {SERVICIOS_AUTH} from './submodulos/submodulo-auth0-movil/constantes/servicios-auth';
import {MostrarLocalModule} from './submodulos/submodulo-auth0-movil/componentes/mostrar-local/mostrar-local.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MapaParadasComponent} from './componentes/mapa-paradas/mapa-paradas.component';

@NgModule({
  declarations: [AppComponent, ModalInfoLocalComponent, MapaParadasComponent],
  entryComponents: [ModalInfoLocalComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    ServiciosAuth0Module,
    MostrarLocalModule,
    BrowserAnimationsModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    HTTP,
    OneSignal,
    ...SERVICIOS_PRINCIPAL,
    ...SERVICIOS_AUTH,
    ScreenOrientation
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
